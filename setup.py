#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = [
    "freckles==1.0.0b1"
]

setup_requirements = ["pytest-runner"]

test_requirements = ["pytest"]

setup(
    author="Markus Binsteiner",
    author_email="markus@frkl.io",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: Other/Proprietary License",
        "Natural Language :: English",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
    ],
    description="Internal freckles utilities.",
    entry_points={
        "console_scripts": ["freckles_utils=freckles_utils.cli:main"],
        "freckles_cli.plugins": [
            "inaugurate=freckles_utils.freckles_cli_inaugurate_plugin:inaugurate"
        ],
    },
    install_requires=requirements,
    license="Parity Public License 6.0.0",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="freckles_utils",
    name="freckles_utils",
    packages=find_packages(include=["freckles_utils"]),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/makkus/freckles_utils",
    version="1.0.0b1",
    zip_safe=False,
)
