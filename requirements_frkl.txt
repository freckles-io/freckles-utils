--extra-index-url https://pkgs.frkl.io/frkl/dev

git+https://gitlab.com/frkl/frutils.git
git+https://gitlab.com/frkl/frkl.git
git+https://gitlab.com/frkl/ting.git
git+https://gitlab.com/freckles-io/freckles.git
#git+https://gitlab.com/freckles-io/freckles-connector-shell.git
git+https://gitlab.com/freckles-io/freckles-adapter-nsbl.git

