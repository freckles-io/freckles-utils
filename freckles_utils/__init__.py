# -*- coding: utf-8 -*-

"""Top-level package for freckles-utils."""

__author__ = """Markus Binsteiner"""
__email__ = "makkus@frkl.io"
__version__ = "1.0.0-beta1"
