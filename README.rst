==============
freckles-utils
==============


.. image:: https://img.shields.io/pypi/v/freckles_utils.svg
           :target: https://pypi.python.org/pypi/freckles_utils
           :alt: pypi

.. image:: https://readthedocs.org/projects/freckles-utils/badge/?version=latest
           :target: https://freckles-utils.readthedocs.io/en/latest/?badge=latest
           :alt: Documentation Status

.. image:: https://gitlab.com/makkus/freckles-utils/badges/develop/pipeline.svg
           :target: https://gitlab.com/makkus/freckles_utils/pipelines
           :alt: pipeline status


.. image:: https://pyup.io/repos/github/makkus/freckles_utils/shield.svg
           :target: https://pyup.io/repos/github/makkus/freckles_utils/
           :alt: Updates




.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
           :target: https://github.com/ambv/black
           :alt: codestyle



Internal freckles utilities.


* License: Parity Public License 2.1.0
* Documentation: https://freckles-utils.readthedocs.io.


Features
--------

* TODO
